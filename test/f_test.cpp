#include "CppUTest/TestHarness.h"
#include "CppUTest/CommandLineTestRunner.h"
#include "CppUTestExt/MockSupport.h"



#include "f.h"

TEST_GROUP(FirstTestGroup)
{
	void teardown() { mock().clear(); }
};

TEST(FirstTestGroup, FirstTest)
{
	mock().expectOneCall("f2");
	int rc = f1();
	LONGS_EQUAL(31, rc);
}

int main(int ac, char** av)
{
    return CommandLineTestRunner::RunAllTests(ac, av);
}