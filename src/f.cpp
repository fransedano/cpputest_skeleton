#include <stdio.h>

#include "f.h"
#include "f2.h"

int f1(void)
{
	int rc;
	puts("Calling F1");
	rc = f2();
	if (rc == 0) {
		return 31;
	} else {
		return 32;
	}
}
