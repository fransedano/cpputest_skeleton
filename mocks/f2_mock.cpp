#include <stdio.h>

#include "f2.h"

#include "CppUTest/TestHarness.h"
#include "CppUTestExt/MockSupport.h"

int f2(void)
{
	mock().actualCall(__FUNCTION__);
	return 0;
}